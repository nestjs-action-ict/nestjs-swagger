import { Body, Controller, Delete, Get, Param, Post, Put, Query, Redirect } from '@nestjs/common';
import { AppService } from './app.service';
import { CustomLogger } from './custom-logger/custom-logger';
import { RequestContextService } from './request-context/request-context.service';
import { ApiInternalServerErrorResponse, ApiNotFoundResponse, ApiOkResponse } from '@nestjs/swagger';
import { Cat } from './cat.interface';
import { CreateCatDto } from './create-cat.dto';
import { UpdateCatDto } from './update-cat.dto';

@Controller()
export class AppController {
  private readonly logger: CustomLogger

  constructor(
    private readonly appService: AppService,
    private readonly requestContextService: RequestContextService
    ) {
    this.logger = new CustomLogger(requestContextService, AppController.name)
  }

  @ApiOkResponse({
    description: 'Retrieved task by ID successfully',
  })
  @ApiNotFoundResponse({ description: 'No task found for ID' })
  @ApiInternalServerErrorResponse({
    description: 'Internal server error',
  })


  @Get('docs')
  @Redirect('https://docs.nestjs.com', 302)
  getDocs(@Query('version') version) {
    if (version && version === '5') {
      return { url: 'https://docs.nestjs.com/v5/' };
    }
  }

  @Get()
  async findAll(): Promise<Cat[]> {
    return this.appService.findAll();
  }

  @Get(':id')
  findOne(@Param() params): string {
    console.log(params.id);
    return `This action returns a #${params.id} cat`;
  }

  @Post()
  create(@Body() createCatDto: CreateCatDto) {
    return 'This action adds a new cat';
  }

  @Put(':id')
  update(@Param('id') id: string, @Body() updateCatDto: UpdateCatDto) {
    return `This action updates a #${id} cat`;
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return `This action removes a #${id} cat`;
  }



}
