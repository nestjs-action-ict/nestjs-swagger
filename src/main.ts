import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { WinstonModule } from 'nest-winston';
import { format, transports } from 'winston';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';



async function bootstrap() {
  const app = await NestFactory.create(AppModule,
    {
      logger: WinstonModule.createLogger({
        exitOnError: false,
        format: format.combine(
          format.colorize(),
          format.timestamp(),
          format.printf(({level, message, timestamp, ...meta}) => {
            if (meta.context && meta.context.id)  {
              return `${timestamp} [${meta.context.id}] ${level} ${meta.context.name} [${meta.context.pid || ''};${meta.context.cid || ''};${meta.context.sid || ''};${meta.context.rid || ''};${meta.context.qid || ''}] - ${message}`;
            }
            return `${timestamp} ${level} - ${message}`;
        })),
        transports: [
          new transports.Console({ level: "debug" })
        ], // alert > error > warning > notice > info > debug
      }),
    });


  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats PI description')
    .setVersion('1.0')
    .addTag('cats')
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('swagger-ui.html', app, document);
  await app.listen(3000);
}
bootstrap();
