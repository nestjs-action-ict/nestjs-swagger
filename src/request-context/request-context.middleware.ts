import { Injectable, NestMiddleware } from '@nestjs/common';
import { RequestContext } from './request-context.model';

@Injectable()
export class RequestContextMiddleware implements NestMiddleware<Request, Response>
{
  use(req: Request, res: Response, next: () => void): any {
    const requestContext = new RequestContext(req, res);
    RequestContext.cls.setContext(requestContext);

    next();

  }

}



